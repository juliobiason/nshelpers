//
//  NSDebug.h
//  PUCRS
//
//  Created by Julio Biason on 19/09/12.
//  Copyright (c) 2012 CWI Software. All rights reserved.
//

#ifdef DEBUG
#   define DBG_LOG_FUNC_ENTER NSLog((@">>> %s"), __PRETTY_FUNCTION__)
#   define DBG_LOG_FUNC_EXIT  NSLog((@"<<< %s"), __PRETTY_FUNCTION__)
#   define DBG_LOG_FUNC_CALL  NSLog((@"=== %s"), __PRETTY_FUNCTION__)
#   define DLog(fmt, ...) NSLog((@"    %s " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__);
#else
#   define DBG_LOG_FUNC_ENTER
#   define DBG_LOG_FUNC_EXIT
#   define DBG_LOG_FUNC_CALL
#   define DLog(...)
#endif
