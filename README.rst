===============
About NSHelpers
===============

NSHelpers is a collection of small Classes/Macros to help developing with
Objective-C, specifically in the Apple environment.

NSDebug.h
---------

NSDebug contains some pre-processor macros to help logging important events.
Because it uses the ``DEBUG`` variable, you can be sure that your final
application won't be slow down or litter the system logs with useless
information.

``DBG_LOG_FUNC_ENTER``
    Marks the start of a function. Generates a message with 
    ">>> *FUNCTION NAME*"

``DBG_LOG_FUNC_EXIT``
	Marks the end of a function. Generates a message with 
	"<<< *FUNCTION NAME*"

``DBG_LOG_FUNC_CALL``
	Marks the single call of a function. Recommended when your function does
	nothing special, like override functions. Generates a message with
	"=== *FUNCTION NAME*"

``DLog``
	Follows the same syntax as NSLog, but prepends the message with 4 spaces
	and the function name (thus alinging the message with the other messages).
	