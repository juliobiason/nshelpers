//
//  NSTimedCache.h
//  PUCRS
//
//  Created by Julio Biason on 10/9/12.
//  Copyright (c) 2012 Julio Biason. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimedCache : NSObject

/**
 Add an object to the cache, valid for the number of milliseconds
 */
+(void) setObject:(id)object forKey:(NSString *)key forMilliseconds:(int)milliseconds;

/**
 Add an object to the cache, valid for the number of seconds
 */
+(void) setObject:(id)object forKey:(NSString *)key forSeconds:(int)seconds;

/**
 Add an object to the cache, valid for the number of days
 */
+(void) setObject:(id)object forKey:(NSString *)key forDays:(int)days;

/**
 Add an object to the cache, valid for the number of weeks
 */
+(void) setObject:(id)object forKey:(NSString *)key forWeeks:(int)weeks;

/**
 Returns an object from the cache. Returns nil if the object doesn't exist or is
 expired.
 */
+(id) objectForKey:(NSString *)key;

/**
 Return an object from the cache. If "validade" is YES, also checks if the object isn't
 expired; NO will remove the expiration check and return the object regardless. Returns
 nil if there is no such object in the cache or if it's expired and "validade" is YES.
 */
+(id) objectForKey:(NSString *)key validate:(BOOL)validate;

/**
 Returns a NSDate with the time the object was last modified in the cache.
 */
+(NSDate *) getUpdateDateForKey:(NSString *)key;

/**
 Destroys the cache
 */
+(void)destroy;

@end
