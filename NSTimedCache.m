//
//  NSTimedCache.m
//  PUCRS
//
//  Created by Julio Biason on 10/9/12.
//  Copyright (c) 2012 Julio Biason. All rights reserved.
//

#import "NSTimedCache.h"
#import "NSDebug.h"

#define SELF_CACHE_KEY @"_timed_cache"

@implementation NSTimedCache

#pragma mark - Funções visíveis

// add an object to the cache, valid for the number of milliseconds
+(void) setObject:(id)object forKey:(NSString *)key forMilliseconds:(int)milliseconds
{
    DBG_LOG_FUNC_ENTER;
    NSUserDefaults      *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *cache = [NSMutableDictionary dictionaryWithDictionary:[defaults objectForKey:SELF_CACHE_KEY]];

    double creation = [[NSDate date] timeIntervalSince1970];
    double expiration = creation + (milliseconds / 1000);

    DLog(@"Key = %@, Creation = %f, Expire = %f", key, creation, expiration);

    NSDictionary *store = [NSDictionary dictionaryWithObjectsAndKeys:
                           object, @"value",
                           [NSNumber numberWithDouble:creation], @"creation",
                           [NSNumber numberWithDouble:expiration], @"expire",
                           nil];
    [cache setObject:store forKey:key];
    [defaults setObject:cache forKey:SELF_CACHE_KEY];
    DBG_LOG_FUNC_EXIT;
}

// add an object to the cache, valid for the number of seconds
+(void) setObject:(id)object forKey:(NSString *)key forSeconds:(int)seconds
{
    [self setObject:object forKey:key forMilliseconds:seconds * 1000];
}

// add an object to the cache, valid for the number of days
+(void) setObject:(id)object forKey:(NSString *)key forDays:(int)days {
    DBG_LOG_FUNC_CALL;
    [self setObject:object forKey:key forSeconds:(days * 24 * 60 * 60)];
}

// add an object to the cache, valid for the number of weeks
+(void) setObject:(id)object forKey:(NSString *)key forWeeks:(int)weeks {
    DBG_LOG_FUNC_CALL;
    [self setObject:object forKey:key forDays:(weeks * 7)];
}

// returns the cache object, as long as it's not expired
+(id) objectForKey:(NSString *)key {
    DBG_LOG_FUNC_CALL;
    return [self objectForKey:key validate:YES];
}

// returns the cached object
+(id) objectForKey:(NSString *)key validate:(BOOL)validate {
    DBG_LOG_FUNC_ENTER;
    NSDictionary *store = [self getStore:key];
    if (store == nil) {
        DLog(@"Key not found: %@", key);
        DBG_LOG_FUNC_EXIT;
        return nil;
    }

    if (validate) {
        double now = [[NSDate date] timeIntervalSince1970];
        double expiration = [[store objectForKey:@"expire"] floatValue];
        if (now > expiration) {
            DLog(@"Key expired: %f / %f", now, expiration);
            DBG_LOG_FUNC_EXIT;
            return nil;
        }
    }

    DBG_LOG_FUNC_EXIT;
    return [store objectForKey:@"value"];
}

// retorna a data de criacao do elemento no cache
+(NSDate *) getUpdateDateForKey:(NSString *)key
{
    DBG_LOG_FUNC_ENTER;
    NSDictionary *store = [self getStore:key];
    if (store == nil) {
        DLog(@"Key not found: %@", key);
        DBG_LOG_FUNC_EXIT;
        return nil;
    }

    DLog(@"store = %p", store);
    DBG_LOG_FUNC_EXIT;
    return [NSDate dateWithTimeIntervalSince1970:[[store objectForKey:@"creation"] doubleValue]];
}

// destroy the cache and its contents
+(void)destroy
{
    DBG_LOG_FUNC_ENTER;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:SELF_CACHE_KEY];
    DBG_LOG_FUNC_EXIT;
    return;
}

#pragma mark - Helpers
+(id) getStore:(NSString *)key {
    DBG_LOG_FUNC_ENTER;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary   *cache    = [defaults objectForKey:SELF_CACHE_KEY];
    if (cache == nil) {
        // nothing in cache yet
        return nil;
    }

    NSDictionary   *store    = [cache objectForKey:key];
    DLog(@"Key = %@, Store = %p", key, store);
    DBG_LOG_FUNC_EXIT;
    return store;
}

@end
